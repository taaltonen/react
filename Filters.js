import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Form from "react-bootstrap/Form";

//default values
var country = "FI";
var year = "2021";

export { country, year };

export default class Filters extends React.Component {
  state = {
    options: [
      {
        name: "Finland",
        value: "FI",
      },
      {
        name: "USA",
        value: "US",
      },
      {
        name: "Sweden",
        value: "SE",
      },
      {
        name: "Norway",
        value: "NO",
      },
      {
        name: "Denmark",
        value: "DK",
      },
      {
        name: "Spain",
        value: "ES",
      },
    ],
    value: "FI",
  };

  handleChange = (event) => {
    this.setState({ value: event.target.value });
    country = event.target.value;
  };

  updateYear = (event) => {
    this.setState({ year: event.target.value });
    year = event.target.value;
  };

  render() {
    const { options, value } = this.state;

    return (
      <>
        <p> </p>
        <Form.Label>Country: {value}</Form.Label>
        <Form.Select onChange={this.handleChange} value={value}>
          {options.map((item) => (
            <option key={item.value} value={item.value}>
              {item.name}
            </option>
          ))}
        </Form.Select>
        <p> </p>
        <Form.Label>Year: {year}</Form.Label>
        <Form.Control
          type="Year"
          placeholder="Years 1900-2049"
          onChange={this.updateYear}
        />
        <p> </p>
      </>
    );
  }
}