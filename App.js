import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Clock from "./Clock.js";
import Data from "./Data.js";
import Select from "./Filters.js";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: true,
      items: [],
    };
  }

  render() {
    return (
      <div className="App">
        <>
          <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container fluid="md">
              <Navbar.Brand>Holiday App</Navbar.Brand>
              <Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="me-auto"></Nav>
                <Nav>
                  <Clock></Clock>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
          <Container fluid="md">
            <p> </p>
            <h1>Holidays</h1>
            <p> </p>
            <Select></Select>
          </Container>
          <p> </p>
          <Container fluid="md">
            <Data></Data>
          </Container>
        </>
      </div>
    );
  }
}
export default App;