# Holiday Web Project

| **Name**        | Holiday-app                                                                                                                                                                                                                                                                                                                                                   |
| --------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Topic**       | Web-application that displays holidays and important dates filtered by country, year, and religion. Possible features include multiple filters, blacklisting with keywords, calculating time until the next or a certain holiday, and possibly filtering by specific weekdays or week numbers. For UI elements mobile compatibility is to be considered as well. |
| ---             | ---                                                                                                                                                                                                                                                                                                                                                              |
| **API**         | https://calendarific.com/api/v2                                                                                                                                                                                                                                                                                                                                  |
| ---             | ---                                                                                                                                                                                                                                                                                                                                                              |
| **Heroku link** | WIP                                                                                                                                                                                                                                                                                                                                                              |

Release 1: 2021-11-15 features

- User is able to see UI with dummy reservation data
- User is able to see the current date on the UI
- User is able to see the filter fields

Release 2: 2021-11-29 features

- User can access the Holiday data
- User can filter the data with inputs
- UI is now compatible with mobile phones
- All console errors taken care of

Known Bugs

- Empty space top right corner using horizontal phone view