import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { country, year } from "./Filters.js";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

const API_KEY = process.env.REACT_APP_API_KEY;
var url =
  "https://calendarific.com/api/v2/holidays?&api_key=" + API_KEY + "&country=";
var aCountry = country;
var url2 = "&year=";
var aYear = year;
var i = 1;

export default class Data extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: true,
      items: [],
    };
    this.updateState = this.updateState.bind(this);
  }

  // Initial API render
  componentDidMount() {
    fetch(url + country + url2 + year)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.response.holidays,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  // Updates data based on user input
  updateState() {
    if (year < 1900 || year > 2049) {
      alert("Choose a valid year between 1900-2049");
    } else {
      if (aCountry !== country || aYear !== year) {
        aCountry = country;
        aYear = year;
        fetch(url + country + url2 + year)
          .then((res) => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                items: result.response.holidays,
              });
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error,
              });
            }
          );
      } else {
        alert("These filters are already active");
      }
    }
  }

  //Renders API content
  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return (
        <tbody>
          <tr>
            <td>Loading...</td>
            <td>Loading...</td>
            <td>Loading...</td>
            <td>Loading...</td>
          </tr>
        </tbody>
      );
    } else {
      return (
        <>
          <Button variant="info" onClick={this.updateState}>
            Update
          </Button>
          <p></p>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Holiday Name</th>
                <th>Holiday description</th>
                <th>Date</th>
                <th>Type</th>
              </tr>
            </thead>
            <tbody>
              {items.map((item) => (
                <tr key={i++}>
                  <td>{item.name}</td>
                  <td>{item.description}</td>
                  <td>{item.date.iso}</td>
                  <td>{item.type}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </>
      );
    }
  }
}